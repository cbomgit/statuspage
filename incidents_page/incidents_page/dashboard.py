from flask import Flask, render_template
from flask import Flask, jsonify, abort, make_response, request, url_for
from flask.ext.httpauth import HTTPBasicAuth
from reqs import *
import string
app = Flask(__name__)

@app.route('/incidents')
def index():
    incidents = sorted(get_incidents(), key = lambda incident: incident.status)
    return render_template('dashboard.html',incidents = incidents)
if __name__ == '__main__':
   app.run(debug = True)
