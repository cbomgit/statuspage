import requests
import json
import sys

token = '?api_token=nujHg3QrkSlP9OUkRpwjA97RhcfIJbHBoE4y0xPFCK1Ocj91NjOBHyLGheBp'
base_url = 'https://dev.cbom.me/api/1.0'
headers={'content-type':'application/json'}

class service:
    id = 0
    name = ''
    current_status = ''
    is_active = 0
    def service_details(self,id):
        url = base_url + '/service/{}'.format(self.id)
        service_info = json.loads(requests.get(url + token, data=None, headers=headers).content.decode('ascii'))
        return service_info['payload']
    def __init__(self,id=id,name=name,current_status=current_status,is_active=is_active):
        self.id = id
        self.name = name
        self.current_status = current_status
        self.is_active = is_active
class incident:
    id = 0
    short_desc = ""
    date = ""
    type = ""
    description = ""
    status = ""
    def incident_details(self):
        url = base_url + '/incident/{}'.format(self.id)
        incident_info = json.loads(requests.get(url + token, data=None, headers=headers).content.decode('ascii'))
        return incident_info['payload']
    def __init__(self,id,short_desc,date,type,description,status):
        self.id = id
        self.short_desc = short_desc
        self.date = date
        self.type = type
        self.description = description
        self.status = status

def get_services():
    services = []
    url = base_url + '/service' + token
    response = json.loads(requests.get(url, data=None, headers=headers).content.decode('ascii'))
    # create an array of service objects
    for s in response['payload']:
        new_service = service(s['id'],s['name'],s['current_status'],s['is_active'])
        services.append(new_service)
    return services

def post_service(name,is_active=True,current_status="OK"):
    payload = {
        'name':name,
        'is_active':is_active,
        'current_status':current_status
    }
    url = base_url + '/service' + token
    response = json.loads(requests.post(url, data=payload, headers=headers).content.decode('ascii'))
    return response

def put_service(id,is_active,name):
    payload = {
        "is_active":is_active,
        "name":name
    }
    url = base_url + '/service/' + str(id) + token
    response = json.loads(requests.put(url, data=payload, headers=headers).content.decode('ascii'))
    return response

def delete_service(id):
    url = base_url + '/service/' + str(id) + token
    response = json.loads(requests.delete(url, data=payload, headers=headers).content.decode('ascii'))
    return response

def get_incidents():
    incidents = []
    url = base_url + '/incident' + token
    response = json.loads(requests.get(url, data=None, headers=headers).content.decode('ascii'))
    # create an array of incident objects
    for i in response['payload']:
        new_incident = incident(i['id'],i['short_desc'],i['date'],i['type'],i['description'],i['status'])
        incidents.append(new_incident)
    return incidents

def post_incident(short_desc,date,type,description,status):
    payload = {
        'short_desc':short_desc,
        'date':date,
        'type':type,
        'description':description,
        'status':status
    }
    url = base_url + '/incident' + token
    response = json.loads(requests.post(url, data=payload, headers=headers).content.decode('ascii'))
    return response
def put_incident(id,is_active,name):
    payload = {
        'is_active':is_active,
        'name':name
    }
    url = base_url + '/incident/' + str(id) + token
    response = json.loads(requests.put(url, data=payload, headers=headers).content.decode('ascii'))
    return response
