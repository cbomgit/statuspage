@extends('layouts.app')

@section('page-styles')

    <style type="text/css">

       /* #active-services-collapse .panel.panel-default {
            border: 1px solid #DB8438;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
        }

        #active-services-collapse .panel-heading {
            background-color: #DB8438;
            color:white;
            -webkit-border-radius:0;
            -moz-border-radius:0;
            border-radius:0;
        }

        #resolved-services-collapse .panel.panel-default{
            border: 1px solid #0eb55e;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
        }

        #resolved-services-collapse .panel-heading {
            background-color: #0eb55e;
            color:white;
            -webkit-border-radius:0;
            -moz-border-radius:0;
            border-radius:0;
        }*/

        .panel-title {
            font-size: 24px;
        }

        .reported-date {
            float: right;
            margin-top: 6px;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-10">
                <h2 style="text-decoration:underline">Services Details Page</h2>

                <div class="panel-group" id="active-services-collapse">
                    @foreach($incidents as $i)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#active-incidents-collapse" href="#active-incident-collapse-{{ $loop->index }}">
                                        {{ $i->short_desc }}</a>
                                    <small>{{ $i->type }}</small>
                                    <small class="reported-date">Date Reported: {{ $i->date }}</small>
                                    <a href="/incident-details/{{ $i->id }}">
                                        <img src="/images/edit-icon-white.png" width="16px" height="auto"/>
                                    </a>
                                </h2>
                            </div>
                            <div id="active-incident-collapse-{{ $loop->index }}" class="panel-collapse collapse {{ $loop->index == 0 ? "in" : "out" }}">
                                <div class="panel-body">
                                    <h4>Status: {{ $i->status }}</h4>
                                    <p> {{ $i->description }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>
    </div>
@endsection