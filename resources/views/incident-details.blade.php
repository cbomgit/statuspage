@extends('layouts.app')

@section('page-styles')
    <link rel="stylesheet" href="/js/lib/jquery-ui/jquery-ui.css"/>
    <link rel="stylesheet" href="/js/lib/timepicker/jquery.timepicker.css"/>
    <style>
        .required-text {
            color: red;
            font-size: .8em;
            font-style: italic;
            display:none;
        }

        .action-links {
            padding-left:10px;
            padding-right:10px;
        }
        ul#service-list {
            list-style-type: none;
            padding: 0px;
            margin-bottom: 7px;
        }

        li.list-item-service {
            border-radius: 20px;
            border: 1px solid grey;
            margin-bottom: 1%;
            padding: 1%;
        }

        li.list-item-service span {
            font-size: 1.2em;
            margin-left: 4%;
        }


        li.selected-service {
            background-color: #4B92DB;
            color: white;
            border: 1px solid #4B92DB;
        }

        h4.sub-heading {
            text-decoration: underline;
        }
        
        #resolution-info {
            display: none;
        }


    </style>
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2>Create a new Incident</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a class="action-links" title="Cancel - go back" onclick="cancel()">
                    <img class="h2" src="/images/cancel-icon.png" alt="Go back" height="32px" width="auto"/>
                </a>
                <a class="action-links" title="Save">
                    <img class="h2" src="/images/save-icon.png" alt="Save" height="32px" width="auto" onclick="save({{ $incident->id }})"/>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-xs-12">
                    <h4 class="sub-heading">Incident Details</h4>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="short-desc">Short Desc</label>
                        <input class="form-control" type="text" name="short-desc" title="Short Description" id="short-desc" value="{{ $incident->short_desc }}">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-date">Reported Date</label>
                        <input class="form-control" type="text" name="incident-date" title="Incident Date" id="incident-date" value="{{ date("Y-m-d", strtotime($incident->date)) }}">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-time">Reported Time</label>
                        <input class="form-control" type="text" name="incident-time" title="Incident Time" id="incident-time" value="{{ date("h:i a", strtotime($incident->date)) }}">
                    </div>
                </div>
                <div class="clearfix visible-lg visible-md visible-sm"></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-type">Type</label>
                        <select class="form-control" name="incident-type" title="Incident Date" id="incident-type">
                            <option {{ $incident->type == "Unplanned Outage" ? "selected" : "" }}>Unplanned Outage</option>
                            <option {{ $incident->type == "Planned Maintenance" ? "selected" : "" }}>Planned Maintenance</option>
                            <option {{ $incident->type == "Emergency Maintenance" ? "selected" : "" }}>Emergency Maintenance</option>
                            <option {{ $incident->type == "General Communication" ? "selected" : "" }}>General Communication</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-status">Current Status</label>
                        <select class="form-control" name="incident-status" title="Incident Status" id="incident-status">
                            <option {{ $incident->status == "N/A" ? "selected" : "" }}>N/A</option>
                            <option {{ $incident->status == "Reported" ? "selected" : "" }}>Reported</option>
                            <option {{ $incident->status == "Investigating" ? "selected" : "" }}>Investigating</option>
                            <option {{ $incident->status == "Resolved" ? "selected" : "" }}>Resolved</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="from-group">
                        <label for="long-description">Detailed Description</label>
                        <textarea class="form-control" rows="8" name="long-description" title="Long Description" id="long-description">{{ $incident->description }}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-xs-12">
                    <h4 class="sub-heading">Impacted Services</h4>
                </div>
                <div class="col-xs-12">
                    <ul id="service-list" class="table table-striped">
                        @foreach($services as $s)
                            <li class="list-item-service unselected-service" data-service-id="{{ $s->id }}" id="service-{{ $s->id }}">
                                <span>{{ $s->name }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div id="resolution-info">
                    <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="resolved-date">Resolved Date</label>
                            <input class="form-control"
                                   type="text" name="incident-resolved-date"
                                   title="Incident Resolved Date"
                                   id="incident-resolved-date" value="{{ date("Y-m-d", strtotime($incident->resolved_date)) }}">
                        </div>
                    </div>
                    <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="resolved-time">Resolved Time</label>
                            <input class="form-control"
                                   type="text" name="incident-resolved-time"
                                   title="Incident Resolved Time"
                                   id="incident-resolved-time" value="{{ date("h:i a", strtotime($incident->date)) }}">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="from-group">
                            <label for="resolution-notes">Resolution Notes</label>
                            <textarea class="form-control" rows="8" name="resolution-notes" title="Resolution Notes" id="resolution-notes">{{ $incident->resolution }}</textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="/js/lib/jquery-ui/jquery-ui.js"></script>
    <script src="/js/lib/moment.min.js"></script>
    <script src="/js/lib/timepicker/jquery.timepicker.js"></script>
    <script src="/js/API.js"></script>
    <script src="/js/incidentFormFields.js"></script>
    <script>

        var impactedServices = {!! json_encode($impactedServices) !!};

        //validates the form for saving
        function canSave() {

            //get the required fields
            var requiredFields = fields.filter(function(item) {
                return item.isRequired();
            });


            var canSave = true;

            requiredFields.forEach(function(item) {

                if(item.hasGoodInput()) {

                    canSave = canSave && true; //don't set canSave to true if it's already false
                    item.hideErrorMessage();

                }
                else {

                    canSave = false;
                    item.showErrorMessage();

                }
            });

            return canSave;
        }

        function save(incidentId) {

            if(canSave()) {

                var api = new API('incident', '{{ $api_token }}');

                var date = fieldsLookup['incident-date'].getValue();
                var time = fieldsLookup['incident-time'].getValue();

                date.hour(time.hours()).minutes(time.minutes());

                var putBody = {
                    short_desc : fieldsLookup['short-desc'].getValue(),
                    date : date.format("YYYY-MM-DD HH:mm:ss"),
                    type: fieldsLookup['incident-type'].getValue(),
                    description: fieldsLookup['long-description'].getValue(),
                    status : fieldsLookup['incident-status'].getValue()
                };

                if(fieldsLookup['incident-status'].getValue() === 'Resolved') {
                    let resolvedDate = fieldsLookup['incident-resolved-date'].getValue();
                    let resolvedTime = fieldsLookup['incident-resolved-time'].getValue();
                    resolvedDate.hour(resolvedTime.hours()).minutes(resolvedTime.minutes());
                    putBody['resolved_date'] = resolvedDate.format("YYYY-MM-DD HH:mm:ss");
                    putBody['resolution'] = fieldsLookup['resolution-notes'].getValue();
                }

                api.put(incidentId, putBody).then(function(result) {

                    window.open('/incidents', '_self');

                }, function(error) {

                    console.log(error);

                });
            }

        }

        function cancel() {

            window.open("/incidents", "_self");

        }

        $(document).ready(function() {
            impactedServices.forEach( (element, index) => {
                $("#service-" +element['pivot']['service'])
                .toggleClass("selected-service")
                .toggleClass("unselected-service");
            });
        })

    </script>
@endsection
