@extends('layouts.app')

@section('page-styles')

    <style type="text/css">

        #active-incidents-collapse .panel.panel-default {
            border: 1px solid #DB8438;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
        }

        #active-incidents-collapse .panel-heading {
            background-color: #DB8438;
            color:white;
            -webkit-border-radius:0;
            -moz-border-radius:0;
            border-radius:0;
        }

        #resolved-incident-collapse .panel.panel-default{
            border: 1px solid #0eb55e;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
        }

        #resolved-incidents-collapse .panel-heading {
            background-color: #0eb55e;
            color:white;
            -webkit-border-radius:0;
            -moz-border-radius:0;
            border-radius:0;
        }

        .panel-title {
            font-size: 24px;
        }

        .reported-date {
            float: right;
            margin-top: 6px;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-10">
                <h2 style="text-decoration:underline">Current Outages</h2>
            </div>
            <div class="col-xs-2">
                <a href="/new-incident">
                    <button id="create-new-button" class="h1 btn btn-primary" type="submit">Create New</button>
                </a>
            </div>
        </div>
        <div class="panel-group" id="active-incidents-collapse">
            @foreach($active as $i)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#active-incidents-collapse" href="#active-incident-collapse-{{ $loop->index }}">
                            {{ $i->short_desc }}</a>
                        <small>{{ $i->type }}</small>
                        <small class="reported-date">Date Reported: {{ $i->date }}</small>
                        <a href="/incident-details/{{ $i->id }}">
                            <img src="/images/edit-icon-white.png" width="16px" height="auto"/>
                        </a>
                    </h2>
                </div>
                <div id="active-incident-collapse-{{ $loop->index }}" class="panel-collapse collapse {{ $loop->index == 0 ? "in" : "out" }}">
                    <div class="panel-body">
                        <h4>Status: {{ $i->status }}</h4>
                        <p> {{ $i->description }}</p>
                        <h4 style="text-decoration:underline">Impacted Services</h4>
                        @if(count($i->impactedServices) == 0)
                        <p>None</p>
                        @else
                        <ul>
                            @foreach($i->impactedServices as $s)
                                <li>{{ $s->name }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-xs-12" style="text-align: center; text-decoration:underline">
                <h2>Past Incidents</h2>
            </div>
        </div>
        <div class="panel-group" id="resolved-incidents-collapse">
            @foreach($resolved as $i)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">
                            <a data-toggle="collapse" data-parent="#resolved-incidents-collapse" href="#resolved-incident-collapse-{{ $loop->index }}">
                                {{ $i->short_desc }}</a>
                            <small>{{ $i->type }}</small>
                            <small class="reported-date">Date Reported: {{ $i->date }}</small>
                        </h2>
                    </div>
                    <div id="resolved-incident-collapse-{{ $loop->index }}" class="panel-collapse collapse out">
                        <div class="panel-body">
                            <h4>Status: {{ $i->status }}</h4>
                            <p>{{ $i->description }}</p>
                            <h4 style="text-decoration:underline">Impacted Services</h4>
                            @if(count($i->impactedServices) == 0)
                                <p>None</p>
                            @else
                                <ul>
                                    @foreach($i->impactedServices as $s)
                                        <li>{{ $s->name }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <h4>Resolved on: <small> {{ $i->resolved_date }}</small></h4>
                            <p>
                                {{ $i->resolution }}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection