@extends('layouts.app')

@section('page-styles')

    <style>
        ul#services-list {
            list-style-type: none;
            padding: 0px;
        }

        #error-text {
            display: none;
            color:red;
        }

        th.first-col, td.first-col {
            text-align: left;
        }

        th.last-col, td.last-col {
            text-align: right;
        }

        th.middle-col, td.middle-col {
            text-align: center;
        }

        .btn-primary {
            color: #fff;
            background-color: #3097d1;
            border-color: #2a88bd;
            vertical-align: top;
        }

        ul.nav.nav-pills {
            padding:5px 0px 10px 0px;
        }

        #modal-error {
            color:red;
            display: none;
        }

        a {
            cursor:pointer;
        }
    </style>

@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-mg-9 col-sm-9 col-xs-7">
                <h1>Services</h1>
                <span id="error-text"></span>
            </div>
            <div class="col-lg-3 col-mg-3 col-sm-3 col-xs-5">
                <button id="create-new-button" class="h1 btn btn-primary" type="submit">Create New</button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12" id="new-service-row" style="display:none">
                <div class="form-inline">
                    <label for="new-service-name">
                        <input placeholder="Service Name" type="text" class="form-control" name="new-service-name" id="new-service-name">
                    </label>
                    <button id="save-service-button" type="submit" class="btn btn-primary" onclick="saveService()">Ok</button>
                </div>
            </div>
            <div class="col-xs-12">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#active-services" data-toggle="tab">Active</a></li>
                    <li><a href="#retired-services" data-toggle="tab">Retired</a></li>
                </ul>
                <div class="tab-content">
                    <div id="active-services" class="tab-pane fade in active">
                    <!-- this is where services will be listed -->
                    <table class="table table-responsive table-striped table-hover">
                        <tr>
                            <th class='first-col'>Name</th>
                            <th class="middle-col">Status</th>
                            <th class='last-col'>Actions</th>
                        </tr>
                        @foreach($obj as $s)
                        <tr class="service-row" data-service-id="{{ $s['service']->id }}">
                            <td class='first-col'>
                                <a href="/service-details/{{$s['service']->id}}">{{ $s['service']->name }}</a>
                            </td>
                            <td class="middle-col">
                                @if($s['service']->current_status == 'OK')
                                    <img title="{{$s['service']->current_status}}" alt="Status Ok" src="/images/ok-status.png" height="32px" width="auto"/>
                                @else
                                    <img title="{{$s['service']->current_status}}" alt="Status Down" src="/images/bad-status.png" height="32px" width="auto"/>
                                @endif
                            </td>
                            <td class='last-col'>
                                <a href="/new-incident/{{ $s['service']->id }}">
                                    <img
                                            class="new-incident-link"
                                            title="Create an incident"
                                            alt="New Incident"
                                            src="/images/add-new-icon.png"
                                            height="24px" width="auto"
                                    />
                                </a>
                                <a>
                                    <img
                                            class="retire-link"
                                            title="Retire this service"
                                            alt="Retire"
                                            src="/images/retire-arrow.jpg"
                                            height="24px" width="auto"
                                            onclick="retireModal('{{$s['service']->id}}', '0')"
                                    />
                                </a>
                                @if(!$s['subscriptionId'])
                                <a onclick="subscribeToService('{{ $s['service']->id }}')">
                                    <img alt="Subscribe to notifications"
                                         title="Subscribe to notifications"
                                         src="/images/subscribe-icon.png"
                                         height="24px" width="auto"
                                     />
                                </a>
                                @else
                                <a onclick="deleteSubscription('{{ $s['subscriptionId'] }}')">
                                    <img alt="Unsubscribe to notifications"
                                         title="Unsubscribe to notifications"
                                         src="/images/unsubscribe-icon.png"
                                         height="24px" width="auto"
                                    />
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div id="retired-services" class="tab-pane fade in">
                    <table class="table table-responsive table-striped table-hover tab-pane">
                        <tr>
                            <th class='first-col'>Name</th>
                            <th class="middle-col">Status</th>
                            <th class="last-col">Actions</th>
                        </tr>
                        @foreach($retired as $s)
                        <tr class="service-row" data-service-id="{{$s->id}}">
                            <td class='first-col'>
                                <a onclick="renameModal('{{  $s->id }}')">{{ $s->name }}</a>
                            </td>
                            <td class="middle-col">
                                @if($s->current_status == 'OK')
                                    <img title="{{$s->current_status}}" alt="Status Ok" src="/images/ok-status.png" height="32px" width="auto"/>
                                @else
                                    <img title="{{$s->current_status}}" alt="Status Down" src="/images/bad-status.png" height="24px" width="auto"/>
                                @endif
                            </td>
                            <td class="last-col">
                                <a>
                                    <img
                                            class="retire-link"
                                            title="Activate this service"
                                            alt="Activate"
                                            src="/images/activate-arrow.jpg"
                                            height="24px" width="auto"
                                            onclick="retireModal('{{$s->id}}', '1')"
                                    />
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal for service retirement/activate -->
    <div id="retire-service-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Activate/Retire this service?</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to retire or activate this service?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" onclick="retireService()">Yes</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal to rename a service -->
    <div id="rename-service-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Rename this service?</h4>
                </div>
                <div class="modal-body">
                    <p>Service name?</p>
                    <p id="modal-error">You must provide a name.</p>
                    <div class="form-inline">
                        <label for="new-service-name">
                            <input placeholder="Service Name" type="text" class="form-control" name="new-service-name" id="rename-input">
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" onclick="renameService()">Yes</button>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('scripts')
    <script src="/js/API.js"></script>
    <script>

        var serviceToRetire = null;
        var activateOrRetire = null;
        var serviceToRename = null;

        function deleteSubscription(subscriptionId) {
            var api = new API('subscriptions', '{{ $api_token }}');
            api.delete(subscriptionId)
                .then(function(result) {
                    window.open('/services', '_self');
                }, function(error) {
                    alert("Error un-subscribing");
                })
        }

        function subscribeToService(serviceId) {
            var requestPayload = {
                'services': [ serviceId ]
            };
            var api = new API('subscriptions', '{{ $api_token }}');
            api.post(requestPayload)
                .then(function(response) {
                    window.open('/services', '_self');
                }, function(error) {
                    $("#error-text").html(error['responseText']);
                });

        }

        function renameService() {

            var newName = $("#rename-input").val();
            if(newName) {

                $("#error-text").html("Saving...").show(200);
                $("#rename-service-modal").modal("hide");

                var api = new API('service', '{{ $api_token }}');

                api.put(serviceToRename, { 'name': newName })
                    .then(function(result) {

                        window.open('/services?q='+(new Date()).getMilliseconds(), '_self');

                    }, function(error) {

                        $("#error-text").html(error['responseText']);

                    });
            }
            else {
                $("#modal-error").show(200);
            }
        }

        function renameModal(id) {
            serviceToRename = id;
            $("#rename-service-modal").modal("show");

        }

        function retireModal(id, retire) {

            serviceToRetire = id;
            activateOrRetire = retire;
            $("#retire-service-modal").modal("show");

        }

        function retireService() {

            $("#retire-service-modal").modal("hide");

            var api = new API('service', '{{ $api_token }}');
            $("#error-text").html("Saving...").show(200);

            api.put(serviceToRetire, {'is_active': activateOrRetire})
                .then(function(result) {

                    window.open("/services?q="+ (new Date()).getMilliseconds(), '_self');

                }, function(error) {

                    var err = "Error: Status: " + error['error'].code + " Message: " + error['error'].message;
                    $("#error-text").html(err).show(200);

                });
        }

        function saveService() {

            var body = {
                'name': $("#new-service-name").val()
            };

            $("#error-text").html("Saving...").show(200);
            var api = new API('service', '{{ $api_token }}');

            api.post(body).then(function(result){

                $("#error-text").hide();
                $("#new-service-row").hide(200);
                window.open("/services?q=1", "_self");

            }, function(error) {

                var err = "Error: Status: " + error['error'].code + " Message: " + error['error'].message;
                $("#error-text").html(err).show(200);

            });
        }

        $('document').ready(function() {

            $("#create-new-button").click(function() {
                $("#new-service-row").val("").show(200);
            });

        });
    </script>
@endsection
