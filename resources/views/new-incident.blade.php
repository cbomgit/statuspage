@extends('layouts.app')

@section('page-styles')
    <link rel="stylesheet" href="/js/lib/jquery-ui/jquery-ui.css"/>
    <link rel="stylesheet" href="/js/lib/timepicker/jquery.timepicker.css"/>
    <style>
        .required-text {
            color: red;
            font-size: .8em;
            font-style: italic;
            display:none;
        }

        .action-links {
            padding-left:10px;
            padding-right:10px;
        }
        ul#service-list {
            list-style-type: none;
            padding: 0px;
        }

        li.list-item-service {
            border-radius: 20px;
            border: 1px solid grey;
            margin-bottom: 1%;
            padding: 1%;
        }

        li.list-item-service span {
            font-size: 1.2em;
            margin-left: 4%;
        }


        li.selected-service {
            background-color: #4B92DB;
            color: white;
            border: 1px solid #4B92DB;
        }
        
        h4.sub-heading {
            text-decoration: underline;
        }


    </style>
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2>Create a new Incident</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a class="action-links" title="Cancel - go back" onclick="cancel()">
                    <img class="h2" src="/images/cancel-icon.png" alt="Go back" height="32px" width="auto"/>
                </a>
                <a class="action-links" title="Save">
                    <img class="h2" src="/images/save-icon.png" alt="Save" height="32px" width="auto" onclick="save()"/>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-xs-12">
                    <h4 class="sub-heading">Incident Details</h4>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="short-desc">Short Desc</label>
                        <input class="form-control" type="text" name="short-desc" title="Short Description" id="short-desc">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-date">Reported Date</label>
                        <input class="form-control" type="text" name="incident-date" title="Incident Date" id="incident-date">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-time">Reported Time</label>
                        <input class="form-control" type="text" name="incident-time" title="Incident Time" id="incident-time">
                    </div>
                </div>
                <div class="clearfix visible-lg visible-md visible-sm"></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-type">Type</label>
                        <select class="form-control" name="incident-type" title="Incident Date" id="incident-type">
                            <option>Unplanned Outage</option>
                            <option>Planned Maintenance</option>
                            <option>Emergency Maintenance</option>
                            <option>General Communication</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="incident-status">Current Status</label>
                        <select class="form-control" name="incident-status" title="Incident Status" id="incident-status">
                            <option>N/A</option>
                            <option>Reported</option>
                            <option>Investigating</option>
                            <option>Resolved</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="from-group">
                        <label for="long-description">Detailed Description</label>
                        <textarea class="form-control" rows="8" name="long-description" title="Long Description" id="long-description"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-xs-12">
                    <h4 class="sub-heading">Impacted Services</h4>
                </div>
                <div class="col-xs-12">
                    <ul id="service-list" class="table table-striped">
                        @foreach($services as $s)
                        <li class="list-item-service {{ isset($service) && $service->name == $s->name ? 'selected-service' : 'unselected-service' }}" data-service-id="{{ $s->id }}">
                            <span>{{ $s->name }}</span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script src="/js/lib/jquery-ui/jquery-ui.js"></script>
    <script src="/js/lib/moment.min.js"></script>
    <script src="/js/lib/timepicker/jquery.timepicker.js"></script>
    <script src="/js/API.js"></script>
    <script src="/js/incidentFormFields.js"></script>
    <script>

        //validates the form for saving
        function canSave() {

            //get the required fields
            var requiredFields = fields.filter(function(item) {
                return item.isRequired();
            });


            var canSave = true;

            requiredFields.forEach(function(item) {

                if(item.hasGoodInput()) {

                    canSave = canSave && true; //don't set canSave to true if it's already false
                    item.hideErrorMessage();

                }
                else {

                    canSave = false;
                    item.showErrorMessage();

                }
            });

            return canSave;
        }
        function save() {

            if(canSave()) {
                var serviceIds  = $(".selected-service").map((i, el) => {
                        return $(el).data("service-id");
                }).get();

                var api = new API('incident', '{{ $api_token }}');

                var date = fieldsLookup['incident-date'].getValue();
                var time = fieldsLookup['incident-time'].getValue();

                date.hour(time.hours()).minutes(time.minutes());

                var postBody = {
                    short_desc : fieldsLookup['short-desc'].getValue(),
                    date : date.format("YYYY-MM-DD HH:mm:ss"),
                    type: fieldsLookup['incident-type'].getValue(),
                    description: fieldsLookup['long-description'].getValue(),
                    status : fieldsLookup['incident-status'].getValue(),
                    impacted_services : serviceIds

                };

                api.post(postBody).then(function(result) {

                    window.open('/incidents', '_self');

                }, function(error) {

                    console.log(error);

                });
            }

        }

        function cancel() {

            window.open("/incidents", "_self");

        }

        $(document).ready(function() {

            $("li.list-item-service").click(function(e) {
                $(this)
                    .toggleClass("selected-service")
                    .toggleClass("unselected-service");
            });

            $("#incident-date").datepicker({
                dateFormat: "yy-mm-dd",
                currentText: "Now",
                showButtonPanel: true,
                appendText: "(yyyy-mm-dd)",
                constrainInput: true,
                changeMonth: true,
                changeYear: true
            });

            $("#incident-time").timepicker({
                "disableTouchKeyboard":true,
                "typeaheadHighlight":true
            });
        });
    </script>
@endsection
