@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>404 Page Not Found</h1>
        <p> Sorry, the resource you are looking for does not exist</p>
    </div>

@endsection


