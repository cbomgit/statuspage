<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">

                <h3>{{ $incident->short_desc }}</h3>
                <p>{{ $incident->type }}</p>
                <p class="reported-date">Date Reported: {{ $incident->date }}</p>

            </h2>
        </div>
        <div>
            <div class="panel-body">
                <h4>Status: {{ $incident->status }}</h4>

                @if($incident->status != 'Resolved')
                    <p> {{ $incident->description }}</p>
                    <h4 style="text-decoration:underline">Impacted Services</h4>
                    @if(count($impactedServices) == 0)
                        <p>None</p>
                    @else
                        <ul>
                            @foreach($impactedServices as $s)
                                <li>{{ $s->name }}</li>
                            @endforeach
                        </ul>
                @endif
                @else
                    <p> Resolved on: {{ $incident->resolved_date }}</p>
                    <p> {{ $incident->resolution }}</p>
                    <p>
                        All Services have resumed normal operations.
                    </p>
                @endif

            </div>
        </div>
    </div>
</div>