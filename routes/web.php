<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$proxy_url = getenv('PROXY_URL');
$proxy_schema = getenv('PROXY_SCHEMA');

if(!empty($proxy_url)) {
	URL::forceRootUrl($proxy_url);
}

URL::forceScheme('https');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//incident routes
Route::get('/incidents', 'IncidentsController@index');
Route::get('/new-incident/{service?}', 'IncidentsController@newIncident');
Route::get('/incident-details/{incidentId}', 'IncidentsController@show');

//services routes
Route::get('/services', 'ServicesController@index');
/* NEW CHANGE */
//Route::get('/new-service'/{})//
//Route::get('/service-details/{serviceId}', 'ServicesController@show');
Route::get('/service-details/{serviceId}', 'ServicesDetailsController@show');


//contact routes
Route::get('/contacts', 'ContactsController@index');
