#!/bin/bash

php artisan make:migration create_incidents_table --create=incidents
php artisan make:migration create_services_table --create=services
php artisan make:migration create_contacts_table --create=contacts
php artisan make:migration create_emails_table --create=email_notifications
php artisan make:migration create_sms_table --create=sms_notifications
php artisan make:migration create_contact_group_table --create=contact_groups
php artisan make:migration create_impacted_services_table --create=impacted_services



