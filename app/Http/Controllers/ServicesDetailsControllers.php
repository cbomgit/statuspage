<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 5/3/17
 * Time: 4:06 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Service;
use App\Incident;

class ServicesDetailsControllers extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the incidents list
     */
    // Simple controller that takes finds services with  realted incedents (service details)
    public function index($serviceId) {

        $allServices = Service::where('is_active', '1')->get();
        if($serviceId) {
            $service = Service::where('id', $serviceId)->with('relatedIncidents')->first();


            if ($serviceId) {
                return view('services-details', [
                    'services_details' => $service
                ]);
            }
        }
        else{
            return view('404');  // Display a 404 Error (Doesn't exist)
        }
}
}