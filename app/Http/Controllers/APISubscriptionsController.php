<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use App\Subscription;
use App\Service;

class APISubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = Subscription::where('user', $request->user()->id);

        // Process event_cat query string
        if($request->get('email')) {
            $emails = explode(",", $request->get('email'));
            $query->where(function($query) use($emails) {
                foreach($emails as $email) {
                    $query = $query->orWhere('email', $email);
                }
            });
        }
        // Process event_cat query string
        if($request->get('service')) {
            $services = explode(",", $request->get('service'));
            $query->where(function($query) use($services) {
                foreach($services as $service) {
                    $query = $query->orWhere('email', $service);
                }
            });
        }

        return APIController::respond($query->with('service', 'user')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = [
            'user' => $request->user()->id
        ];

        $created = [];

        if($request->has('services')) {
            foreach($request->input("services") as $service) {
                $data['service'] = $service;
                $s =  Service::find($service);
                if(!$s) {
                    return APIController::respondError("Service ".$service." not found");
                }
                $subscription = new Subscription($data);
                $subscription->save();
                $created[] = $subscription;
            }
        }

        return APIController::respond($created);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $sub = Subscription::find($id);
        if(!$sub) {
            return APIController::respondNotFound();
        }

        if($sub->user != $request->user()->id) {
            return APIController::respondUnauthorized();
        }

        $sub = Subscription::where('id', $id)->with('user','service')->first();
        return APIController::respond($sub);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return APIController::respondError('Method not allowed', Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $sub = Subscription::find($id);
        if(!$sub) {
            return APIController::respondNotFound();
        }

        $sub->delete();
        return APIController::respond($sub);
    }
}
