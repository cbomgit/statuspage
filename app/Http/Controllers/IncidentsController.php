<?php

namespace App\Http\Controllers;

use App\Incident;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Translation\Loader\IcuDatFileLoader;

class IncidentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the incidents list
     */
    public function index() {
        $api_token = Auth::user()->api_token;
        $activeIncidents = Incident::orderBy('date', 'DESC')
            ->with('impactedServices')
            ->where('status', '!=', 'Resolved')
            ->get();

        $resolvedIncidents = Incident::orderBy('date', 'DESC')
            ->with('impactedServices')
            ->where('status', 'Resolved')
            ->get();

        return view('incidents', [
            'api_token' => $api_token,
            'active' => $activeIncidents,
            'resolved' => $resolvedIncidents
        ]);
    }

    public function newIncident($service = null) {

        $allServices = Service::where('is_active', '1')->get();
        if($service) {
            $service = Service::find($service);

            if($service) {
                return view('new-incident', [
                    'service' => $service,
                    'services' => $allServices,
                    'api_token' => Auth::user()->api_token
                ]);
            }
        }
        else {
            return view('new-incident', [
                'services' => $allServices,
                'api_token' => Auth::user()->api_token
            ]);
        }

        return view('new-incident');
    }


    public function show($incidentId) {

        $incident = Incident::find($incidentId);
        $allServices = Service::where('is_active', '1')->get();

        if(!$incident) {
            return view('404');
        }

        return view ('incident-details', [
            'incident' => $incident,
            'impactedServices' => $incident->impactedServices,
            'services' => $allServices,
            'api_token' => Auth::user()->api_token
        ]);
    }
}
