<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the incidents list
     */
    public function index() {
        $api_token = Auth::user()->api_token;

        return view('contacts', [
            'api_token' => $api_token
        ]);
    }
}
