<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Service;

class ServicesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the incidents list
     */
    public function index(Request $request) {

        $api_token = Auth::user()->api_token;

        $active = Service::orderBy('name')->where('is_active', '1')->get();
        $retired = Service::orderBy('name')->where('is_active', '0')->get();
        $activeServices = [];

        foreach($active as $a) {
            $sub = Subscription::where([['user', $request->user()->id], ['service', $a->id]])->first();
            if($sub) {
                $temp = [
                    'service' => $a,
                    'subscriptionId' => $sub->id
                ];
            }else {
                $temp = [
                    'service' => $a,
                    'subscriptionId' => null
                ];
            }

            $activeServices[] = $temp;
        }

        return view('services', [
            'api_token' => $api_token,
            'services' => $active,
            'retired' => $retired,
            'obj' => $activeServices
        ]);
    }

/*         NEW CHANGE

    public function show($serviceId) {
//        $service = Service::find($serviceID);
//        $allServices = Service::where('is_active', '1')->get();
        $service = Service::where('id', $serviceId)->with('relatedIncidents')
        if(!$service) {
            return view('404');
        }

        else {
            return view('service-details',[
                'service'=>$service
            ]);
        }

    }
*/

}
