<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Service;

class APIServiceController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return APIController::respond($services);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the request and create the resource
        $data = [
            'name' => $request->get('name'),
            'current_status' => 'Ok',
            'is_active' => true
        ];

        $result = Service::create($data);

        if($result) {
            return APIController::respond($result);
        }
        else {
            return APIController::respondError($result);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $service = Service::find($id);

        if($service) {
            $service = Service::where('id', $id)->with('relatedIncidents', 'subscribed')->first();
            return APIController::respond([ 'payload' => $service ]);
        }
        else {
            return APIController::respondNotFound();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $service = Service::find($id);

        if(!$service) {
            return APIController::respondNotFound();
        }
        $data = [];
        if($request->has('name')) {
            $data['name'] = $request->get('name');
        }

        if($request->has('current_status')) {
            $data['current_status'] = $request->get('current_status');
        }

        if($request->has('is_active')) {
            $data['is_active'] = $request->get('is_active');
        }

        $validator = Service::validateUpdate($data);

        if(!$validator->passes()) {
            return APIController::respondError($validator->errors());
        }

        $isModified = false;

        if($request->has('name')) {
            $service->name = $request->get('name');
            $isModified = true;
        }

        if($request->has('current_status')) {
            $service->current_status = $request->get('current_status');
            $isModified = true;
        }

        if($request->has('is_active')) {
            $service->is_active = $request->get('is_active');
            $isModified = true;
        }

        if($isModified) {
            $service->save();
        }



        return APIController::respond($service);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $service = Service::find($id);

        if(!$service) {
            return APIController::respondNotFound();
        }

        $service->delete();
        return APIController::respond(['id' => $id]);
    }
}
