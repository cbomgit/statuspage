<?php

namespace App\Http\Controllers;

use App\Mail\IncidentNotification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Service;
use App\Incident;
use Mockery\Exception;

class APIIncidentController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Incident::with('impactedServices')->get();
        return APIController::respond($services);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the request and create the resource
        $data = [
            'short_desc' => $request->input('short_desc'),
            'date' => $request->input('date'),
            'type' => $request->input('type'),
            'status' => $request->input('status'),
            'description' => $request->input('description')
        ];

        $validator = Incident::validateCreate($data);
        if(!$validator->passes()) {
            return APIController::respondError($validator->errors());
        }

        $incident = new Incident($data);
        $incident->save();

        if($request->has('impacted_services')) {

            $impacted_services = [];

            foreach($request->input("impacted_services") as $svc) {
                $service = Service::find($svc);
                $service->current_status = 'Service Interruption';
                $service->save();
                $impacted_services[] = ['incident' => $incident->id, 'service' => $svc, 'status' => 'Service Interruption' ];
            }

            try {
                $incident->impactedServices()->attach($impacted_services);
                $incident->save();
            }
            catch(Exception $exception) {
                APIController::respondError($exception);
            }
        }

        return APIController::respond($incident);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $incident = Incident::find($id);


        if($incident) {
            $response = $incident;
            $response['impacted_services'] = $incident->impactedServices;
            return APIController::respond($response);
        }
        else {
            return APIController::respondNotFound();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $incident = Incident::find($id);

        if(!$incident) {
            return APIController::respondNotFound();
        }
        //validate the request and create the resource
        $data = [
            'short_desc' => $request->input('short_desc'),
            'date' => $request->input('date'),
            'type' => $request->input('type'),
            'status' => $request->input('status'),
            'description' => $request->input('description')
        ];

        $validator = Incident::validateUpdate($data);

        if(!$validator->passes()) {
            return APIController::respondError($validator->errors());
        }

        if($request->has('status')) {

            if($request->input('status') == 'Resolved' && $incident->status != 'Resolved') {
                //if the incident is being marked as resolved, mark each impacted service as resolved
                foreach($incident->impactedServices as $svc) {
                    $service = Service::find($svc->pivot->service);
                    $service->current_status = "OK";
                    $service->save();
                }

                $data['resolution'] = $request->input('resolution');
                $data['resolved_date'] = $request->input('resolved_date');
            }

        }

        $incident->update($data);
        //$incident->save();

        return APIController::respond($incident);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $incident = Incident::find($id);

        if(!$incident) {
            return APIController::respondNotFound();
        }

        $incident->delete();
        return APIController::respond(['id' => $id]);
    }
}
