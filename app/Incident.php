<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Incident
 *
 * @property int $id
 * @property string $date
 * @property string $type
 * @property string $description
 * @property string $status
 * @property string $resolved_date
 * @property string $resolution
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method status \Illuminate\Database\Query\Builder|\App\Incident where($col, $val)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereResolution($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereResolvedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Incident whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Incident extends Model
{
    protected $fillable = [
        'short_desc',
        'date',
        'type',
        'description',
        'status',
        'resolved_date',
        'resolution'
    ];

    public static  function validateCreate($data) {

        return Validator::make($data, [
            'short_desc' => 'required|min:3|max:35',
            'is_active' => 'boolean'
        ]);

    }

    public static  function validateUpdate($data) {

        return Validator::make($data, [
            'is_active' => 'boolean',
            'name' => 'min:3|max:35'
        ]);

    }

    public function impactedServices() {
        return $this->belongsToMany('App\Service', 'impacted_services', 'incident', 'service');
    }
}
