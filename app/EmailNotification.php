<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EmailNotification
 *
 * @property int $id
 * @property int $incident
 * @property string $subject
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\EmailNotification whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailNotification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailNotification whereIncident($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailNotification whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EmailNotification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmailNotification extends Model
{
    //
}
