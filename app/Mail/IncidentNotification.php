<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IncidentNotification extends Mailable
{
    use Queueable, SerializesModels;
    protected $emailAddress;
    protected $incident;
    protected $impactedServices;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $inc, $svc)
    {
        //

        $this->emailAddress = $email;
        $this->incident = $inc;
        $this->impactedServices = $svc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('incident-notification')
            ->with([
                'email' => $this->emailAddress,
                'incident' => $this->incident,
                'impactedServices' => $this->impactedServices
            ]);
    }
}
