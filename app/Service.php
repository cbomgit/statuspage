<?php

namespace App;

use App\ImpactedService;
use App\Incident;
use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Service
 *
 * @property int $id
 * @property string $name
 * @property boolean $is_active
 * @property string $current_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service whereCurrentStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Service extends Model
{
    protected $fillable = [
        'name',
        'is_active',
        'current_status'
    ];

    public static  function validateCreate($data) {

        return Validator::make($data, [
            'name' => 'required|min:3|max:35',
            'is_active' => 'boolean'

        ]);

    }

    public static  function validateUpdate($data) {

        return Validator::make($data, [
            'is_active' => 'boolean',
            'name' => 'min:3|max:35'
        ]);
    }

    public static function create($data) {

        $validator = Service::validateCreate($data);
        if(!$validator->passes()) {
            return $validator->errors();
        }


        $service = new Service;
        $service->name = $data['name'];
        $service->current_status = 'OK';
        $service->is_active = true;
        $service->save();

        return $service;

    }

    public function relatedIncidents() {
        return $this->belongsToMany('App\Incident', 'impacted_services', 'service', 'incident');
    }

    public function subscribed() {
        return $this->hasMany('App\Subscription', 'service', 'id');
    }
}
