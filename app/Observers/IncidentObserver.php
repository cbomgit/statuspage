<?php
/**
 * Created by PhpStorm.
 * User: boman
 * Date: 5/4/2017
 * Time: 11:02 AM
 */

namespace App\Observers;
use App\Incident;
use App\User;
use App\Subscription;
use App\Service;
use App\Mail\IncidentNotification;
use Illuminate\Support\Facades\Mail;

class IncidentObserver
{
    public function saved(Incident $incident) {

        $impactedServices = $incident->impactedServices;


        foreach($impactedServices as $s) {

            $subscriptions = $s->subscribed;

            foreach($subscriptions as $subscription) {

                $email = User::find($subscription->user)->email;

                //send email here
                //$email = $subscription->user->email;

                Mail::to($email)->send(new IncidentNotification($email, $incident, $impactedServices));
                

            }
        }
    }

    public function created(Incident $incident) {

        $impactedServices = $incident->impactedServices;


        foreach($impactedServices as $s) {

            $subscriptions = $s->subscribed;

            foreach($subscriptions as $subscription) {

                $email = User::find($subscription->user)->email;

                //send email here
                //$email = $subscription->user->email;

                Mail::to($email)->send(new IncidentNotification($email, $incident, $impactedServices));


            }
        }
    }
}