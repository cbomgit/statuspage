<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ImpactedService
 *
 * @property int $id
 * @property int $incident
 * @property int $service
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\ImpactedService whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ImpactedService whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ImpactedService whereIncident($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ImpactedService whereService($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ImpactedService whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ImpactedService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ImpactedService extends Model
{
    protected $fillable = [
        'status',
        'incident',
        'service'
    ];
    //
    public static  function validateCreate($data) {

        return Validator::make($data, [
            'incident' => 'required|numeric',
            'service' => 'required|numeric'
        ]);

    }

    public static  function validateUpdate($data) {

        return Validator::make($data, [
            'incident' => 'required|numeric',
            'service' => 'required|numeric'
        ]);

    }
}
