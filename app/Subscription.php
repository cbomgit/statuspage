<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Subscription extends Model
{
    //
    protected $fillable = [
        'user',
        'service'
    ];

    public static  function validateCreate($data) {

        return Validator::make($data, [
            'service' => 'required'
        ]);

    }

    public static  function validateUpdate($data) {

        return Validator::make($data, [
            'service' => 'required'
        ]);

    }

    public function service() {
        return $this->belongsTo('App\Service', 'service');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user');
    }
}
