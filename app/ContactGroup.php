<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ContactGroup
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\ContactGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactGroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ContactGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactGroup extends Model
{
    //
}
