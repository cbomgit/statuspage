/**
 * Created by christian on 4/3/17.
 */
/**
 * Created by boman on 3/23/2017.
 */
var fields = [];
var fieldsLookup = {};

$(document).ready(function() {
    fields = [
        new Field({
            name:"short-desc",
            required: true,
            selector:"#short-desc",
            getValue:function() {
                return $(this.selector).val();
            },
            setValue: function(newDesc) {
                $(this.selector).val(newDesc);
            },
            hasGoodInput: function(){
                return this.getValue() != '';
            },
            errorMessage: 'Event title is required'
        }),
        new Field({
            name:"incident-date",
            required: true,
            selector: "#incident-date",
            init: function() {
                $(this.selector).datepicker({
                    dateFormat: "yy-mm-dd",
                    currentText: "Now",
                    showButtonPanel: true,
                    appendText: "(yyyy-mm-dd)",
                    constrainInput: true,
                    changeMonth: true,
                    changeYear: true
                });
            },
            getValue:function() {
                return moment($(this.selector).datepicker("getDate"));
            },
            setValue: function(dateMoment) {
                if(dateMoment.isValid()) {
                    $(this.selector).datepicker("setDate", dateMoment.format("YYYY-MM-DD"));
                }
            },
            hasGoodInput: function() {
                var currentInput  = $(this.selector).datepicker("getDate");
                return  currentInput !== null && moment(currentInput).isValid();
            },
            errorMessage: 'Incident date is required'
        }),
        new Field({
            name:"incident-time",
            required: true,
            selector: "#incident-time",
            init: function() {
                $(this.selector).timepicker({
                    "disableTouchKeyboard":true,
                    "typeaheadHighlight":true
                });


            },
            getValue:function() {
                return moment($(this.selector).timepicker("getTime"));
            },
            setValue: function(timeMoment) {
                if(timeMoment.isValid()) {
                    $(this.selector).datepicker("setTime", timeMoment.format("hh:mm a"));
                }
            },
            hasGoodInput: function() {
                var currentInput  = $(this.selector).timepicker("getTime");
                return  currentInput !== null && moment(currentInput).isValid();
            },
            errorMessage: 'Incident time is required'
        }),
        new Field({
            name:"incident-type",
            required: true,
            selector: "#incident-type",
            getValue:function() {
                return $(this.selector).val();
            },
            hasGoodInput: function() {
                return this.getValue() !== '';
            },
            errorMessage: "This is required"
        }),
        new Field({
            name:"incident-status",
            required: true,
            selector: "#incident-status",
            getValue:function() {
                return $(this.selector).val();
            },
            hasGoodInput: function() {
                return this.getValue() != '';
            },
            errorMessage: "This is required",
            onChange: function() {
                if($("#incident-status").val() === 'Resolved') {
                    $("#resolution-info").show(200);
                }
                else {
                    $("#resolution-info").hide(200);
                }
            },
            init: function() {
                this.onChange();
            }
        }),
        new Field({
            name:"long-description",
            required: true,
            selector: "#long-description",
            getValue:function() {
                return $(this.selector).val();
            },
            hasGoodInput: function() {
                return this.getValue() != '';
            },
            errorMessage: "This is required"
        }),
        new Field({
            name:"incident-resolved-date",
            required: false,
            selector: "#incident-resolved-date",
            init: function() {
                $(this.selector).datepicker({
                    dateFormat: "yy-mm-dd",
                    currentText: "Now",
                    showButtonPanel: true,
                    appendText: "(yyyy-mm-dd)",
                    constrainInput: true,
                    changeMonth: true,
                    changeYear: true
                });
            },
            getValue:function() {
                return moment($(this.selector).datepicker("getDate"));
            },
            setValue: function(dateMoment) {
                if(dateMoment.isValid()) {
                    $(this.selector).datepicker("setDate", dateMoment.format("YYYY-MM-DD"));
                }
            },
            hasGoodInput: function() {
                var currentInput  = $(this.selector).datepicker("getDate");
                return  currentInput !== null && moment(currentInput).isValid();
            },
            errorMessage: 'Resolved date is required'
        }),
        new Field({
            name:"incident-resolved-time",
            required: true,
            selector: "#incident-resolved-time",
            init: function() {
                $(this.selector).timepicker({
                    "disableTouchKeyboard":true,
                    "typeaheadHighlight":true
                });


            },
            getValue:function() {
                return moment($(this.selector).timepicker("getTime"));
            },
            setValue: function(timeMoment) {
                if(timeMoment.isValid()) {
                    $(this.selector).datepicker("setTime", timeMoment.format("hh:mm a"));
                }
            },
            hasGoodInput: function() {
                var currentInput  = $(this.selector).timepicker("getTime");
                return  currentInput !== null && moment(currentInput).isValid();
            },
            errorMessage: 'Resolved time is required'
        }),
        new Field({
            name:"resolution-notes",
            required: false,
            selector: "#resolution-notes",
            getValue:function() {
                return $(this.selector).val();
            },
            hasGoodInput: function() {
                return this.getValue() != '';
            },
            errorMessage: "This is required"
        })
    ];

    //create lookup table
    fieldsLookup = {};
    for(var i = 0; i < fields.length; i++) {
        fieldsLookup[fields[i].name] = fields[i];
    }
});

