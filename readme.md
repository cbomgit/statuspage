# Status Page Demo App #

## Description ##

This is an example status page application that shows the status of various services offered by a company/organization. Users can subscribe to notifications when an event occurs that affects a service. Admins can create new services and incidents. 

## Requirements ##

This app uses Laravel as a framework and requires the following: 
* Apache or some other web server
* php 7.0
* mysql

## Setup ##

1. Set up mysql, php, and your web server
2. Clone this repo
3. Edit your .env file as necessary
4. Run composer install/require to install Laravel and dependencies
5. Run php artisan migrate to set up database