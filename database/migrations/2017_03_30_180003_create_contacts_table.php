<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {

            $table
                ->increments('id');

            //full name of this contact
            $table
                ->string('name');

            //contact email - must be unique
            $table
                ->string('email')
                ->unique();

            //contact phone
            $table
                ->string('phone');

            //contact department
            $table
                ->string('division')
                ->nullable();

            //contact location
            $table
                ->string('location')
                ->nullable();

            //contact group
            $table
                ->integer('group')
                ->unsigned()
                ->nullable();

            $table->foreign('group')
                ->references('id')
                ->on('contact_groups');

            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
