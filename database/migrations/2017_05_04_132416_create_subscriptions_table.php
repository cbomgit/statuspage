<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //email address for this subscription
            $table->integer('user')
                ->unsigned();

            $table->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            //service to receives notifications for
            $table
                ->integer('service')
                ->unsigned();

            $table
                ->foreign('service')
                ->references('id')->on('services')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
