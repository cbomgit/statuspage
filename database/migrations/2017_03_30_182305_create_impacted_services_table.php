<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImpactedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impacted_services', function (Blueprint $table) {

            //service id
            $table
                ->increments('id');

            //incident that impacted this service
            $table
                ->integer('incident')
                ->unsigned();

            $table
                ->foreign('incident')
                ->references('id')->on('incidents')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            //the impacted service
            $table
                ->integer('service')
                ->unsigned();

            $table
                ->foreign('service')
                ->references('id')->on('services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            //current service status during incident
            $table
                ->enum('status', [
                    'OK',
                    'Service Degraded',
                    'Down for Maintenance',
                    'Service Interruption'])
                ->nullable();;

            //date time created
            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('impacted_services');
    }
}
