<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {

            //service id
            $table
                ->increments('id');

            //service name
            $table
                ->string('name')
                ->unique();

            //current service status
            $table
                ->enum('current_status', [
                        'OK',
                        'Service Degraded',
                        'Down for Maintenance',
                        'Service Interruption'])
                ->nullable();

            //service is active or retired
            $table
                ->boolean('is_active')
                ->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
