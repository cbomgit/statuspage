<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {

            $table
                ->increments('id');

            //short description of the incident
            $table
                ->string('short_desc', 100);

            //date incident first reported
            $table
                ->dateTime('date');

            //type of incident
            $table
                ->enum('type', [
                    'Unplanned Outage',
                    'Planned Maintenance',
                    'Emergency Maintenance',
                    'General Communication'
                ]);

            //description of incident
            $table
                ->string('description', 700);

            //overall incident of service
            $table
                ->enum('status', [
                    'N/A',
                    'Reported',
                    'Investigating',
                    'Resolved'
                ]);

            //date incident was resolved
            $table
                ->dateTime('resolved_date')
                ->nullable();

            //resolution notes
            $table->string('resolution')
                ->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
