<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_notifications', function (Blueprint $table) {

            $table->increments('id');

            //incident this notification is for
            $table
                ->integer('incident')
                ->unsigned();

            $table
                ->foreign('incident')
                ->references('id')->on('incidents');

            $table
                ->string('subject');

            $table
                ->string('content');

            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_notifications');
    }
}
