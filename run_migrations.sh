#!/bin/bash

#roll back and rerun all migrations
#php artisan migrate:refresh

#generate the models if they don't exist

php artisan make:model Incident
php artisan make:model Service
php artisan make:model Contact
php artisan make:model EmailNotification
php artisan make:model SMSNotification
php artisan make:model ContactGroup
php artisan make:model ImpactedService

php artisan ide-helper:models
